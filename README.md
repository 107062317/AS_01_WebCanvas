﻿# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets(ex.colorful pen)                 | 1~5%     | Y         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.
#### Tool:選擇哪個工具，該個button會變色以表示現在選擇的工具，鼠標也會改成該圖示。
1. Pen
   一開始**打開網站時會工具自動設成Pen**，鼠標為![](https://i.imgur.com/pnmJrgR.png)，點選到Pen時鼠標亦會更改成上圖。Pen可以在canvas上**自由畫線**，當mousedown開始畫線，直到mouseup/mouseout停止。
2. Eraser
   點選到Eraser時鼠標為![](https://i.imgur.com/TkZabza.png)。Eraser可以在canvas上**自由擦掉任意顏色與線條**，當mousedown開始擦拭，直到mouseup/mouseout停止。
3. Font
   點選到Font時鼠標為![](https://i.imgur.com/KQaU2up.png)。Font可以在canvas上輸入文字（中英文皆可），當mousedown會叫出一個textbox，**先點選textbox**，後輸入想要輸入的文字，**按下鍵盤的"Enter"鍵**即在textbox附近可放上文字，若輸入的過程中mouse按到canvas的其他地方則會在新地方產生textbox，原先的textbox就會消失。**文字的大小以及字體會在產生textbox時存取數值。**
4. Circle
   點選到Circle時鼠標為![](https://i.imgur.com/2EaqCYs.png)。Circle可以在canvas上劃出圓，**圓的大小和方向隨著鼠標位置改變**，當mousedown以按下之處為比較點並開始繪製，直到mouseup/mouseout停止，圓即會出現在canvas上。
5. Rectangle
   點選到Rectangle時鼠標為![](https://i.imgur.com/nHDneXq.png)。Rectangle可以在canvas上劃出長方形，**長方形的大小和方向隨著鼠標位置改變**，當mousedown以按下之處為比較點並開始繪製，直到mouseup/mouseout停止，長方形即會出現在canvas上。
6. Triangle
   點選到Triangle時鼠標為![](https://i.imgur.com/Gfu3zzg.png)。Triangle可以在canvas上劃出三角形，**三角形的大小和方向隨著鼠標位置改變**，當mousedown以按下之處為比較點並開始繪製，直到mouseup/mouseout停止，三角形即會出現在canvas上。比較特別的是當鼠標在比較點的上方為倒三角形，下方則是一般三角形。
7. Line
   點選到Line時鼠標為![](https://i.imgur.com/N2AA07y.png)。Line可以在canvas上劃出直線，當mousedown開始繪製，直到mouseup/mouseout停止，直線即會出現在canvas上。

#### Other: 可以調整筆的粗細、顏色以及控制canvas的功能
1. Undo
   按下Undo，canvas會回到上一個步驟（無論是畫圖或是upload圖）的樣子。如果在**Delete之後按下Undo則不會回到Delete前**的樣子。
2. Redo
   按下Redo，canvas會到曾被記錄過的下一個步驟的樣子（有按過Undo此步驟才有效），若當前位置為最新則保持原樣。
3. Download
   按下Download，網頁會將目前畫布的樣子下載為一張**名為"image.png"的png圖**。
4. Upload
   按下Upload，網頁會將該圖以原本的大小上傳到canvas左上角為起始的位置，且**無論上傳圖的大小為何，canvas都不會改變長寬**。
5. Delete
   按下Delete，**網頁會先詢問是否要進行這一步驟**，按下確定則會將canvas清空，回到初始狀態（無法Undo先前步驟）。
6. Hollow/Solid
   按下Hollow/Solid，可以選擇目前Circle/Rectangle/Triangle圖形是要**空心還是實心，以按鍵在兩者狀態間切換**。
7. Fontsize
   按下Fontsize選單可以選擇輸入文字的字體大小，有不同的大小可以選擇。
8. Font
   按下Font選單可以選擇輸入文字的字體，有不同的字體（有中英文字體）可以選擇。
9. Linewidth
   滑動Linewidth可以改變Pen、Eraser、Circle空心、Rectangle空心、Triangle空心、Line、Colorful pen的線條粗細。有1~30 px，以1為單位可供選擇。
10. Color
    按下Color，可以選擇顏色。此顏色可應用於Pen、Eraser、Font、Circle、Rectangle、Triangle、Line上。

### Function description

    Decribe your bouns function and how to use it.
1. Colorful pen
   點選到Colorful pen時鼠標為![](https://i.imgur.com/ZVQnEd8.png)。Colorful pen可以在canvas上自由畫出彩色的線條，從紅到紫，當mousedown開始畫線，直到mouseup/mouseout停止。
   ![](https://i.imgur.com/jCYz91p.png)
   這個是實作colorful pen的code，是以hsl的方式設定顏色，使用其中的hue，從0一直加到360再歸零的方式。


### Gitlab page link

    your web page URL, which should be  "https://[studentID].gitlab.io/AS_01_WebCanvas"
https://107062317.gitlab.io/AS_01_WebCanvas/

### Others (Optional)

    Anythinh you want to say to TAs.
從這次作業中學習到了許多關於canvas相關的用法，善用網路資源也是一個不錯的學習方式。在這個資料夾中的reference.md有這次作業有實作到功能的參考網路資源，以記錄哪些功能是在哪裡學習到的！

<!--<style>-->
<!--table th{-->
<!--    width: 100%;-->
<!--}-->
<!--</style>-->
