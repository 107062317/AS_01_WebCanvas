//set canvas varialbe
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d')
//flag for drawing or not 
var isDrawing = false;
//flag for writing or not 
var isWriting = false;
var isFinished = true;
//flag for hollow or not
var isHollow = true;
//last position
var lastX = 0;
var lastY = 0;
//color
var hue = 0;
//button id
var id_number = "pen";
//get slide value
var temp_linewidth = document.getElementById('linewidth');
var temp_fontsize = document.getElementById('fontsize');
var temp_font = document.getElementById('fontselector');
var temp_color = document.getElementById('allcolor');
//redo undo variable
var p_array = new Array();
var step = -1;
var lastPic;

async function Draw(e) {
    if (!isDrawing) return;
    if (isWriting){
        CleanTextbox();
    }
    if(id_number == "pen"){
        Pen(e);
    }
    else if(id_number == "colorful"){
        Colorful(e);
    }
    else if(id_number == "eraser"){
        Eraser(e);
    }
    else if(id_number == "circle"){    
        await Animation();
        Circle(e);
    }
    else if(id_number == "triangle"){
        await Animation();
        Triangle(e);
    }
    else if(id_number == "rectangle"){
        await Animation();
        Rectangle(e);
    }
    else if(id_number == "line"){
        await Animation();
        Line(e);
    }
    else if(id_number == "font"){
        if(!isFinished) Font(e);
    }
    
}

//mouse situation
canvas.addEventListener('mousemove', Draw);
canvas.addEventListener('mousedown', (e) => {
    ctx.globalCompositeOperation = 'source-over';
    if(step == -1){
        Push();
    }
    if(isWriting){
        isWriting = false;
        CleanTextbox();
    }
    else{
        if(id_number == "font"){
            isFinished = false;
        }
    }
    // temp_canvas = canvas.toDataURL();
    isDrawing = true;
    [lastX, lastY] = [e.offsetX, e.offsetY];
    //ctx.fillRect(lastX,lastY,1,1);
    Draw(e);
  });
canvas.addEventListener('mouseup', (e) => {
    if(!isDrawing) return;
    isDrawing = false;
    if(id_number == "pen" || id_number == "colorful" || id_number == "eraser" || id_number == "circle" || id_number == "rectangle" || id_number == "triangle" || id_number == "line")Push();
    });
canvas.addEventListener('mouseout', (e) => {
    ctx.closePath();
    if(!isDrawing) return;
    isDrawing = false;
    if(id_number == "pen" || id_number == "colorful" || id_number == "eraser" || id_number == "circle" || id_number == "rectangle" || id_number == "triangle" || id_number == "line")Push();
    });

canvas.style.cursor = `url('./icon_svg/pen.svg'), auto`;

function Clickitem(number){
    id_number = number;
    if(id_number == "pen"){
        canvas.style.cursor = `url('./icon_svg/pen.svg'), auto`;
    }
    else if(id_number == "colorful"){
        canvas.style.cursor = `url('./icon_svg/colorful.svg'), auto`;
    }
    else if(id_number == "eraser"){
        canvas.style.cursor = `url('./icon_svg/eraser.svg'), auto`;
    }
    else if(id_number == "circle"){
        canvas.style.cursor = `url('./icon_svg/circle.svg'), auto`;
    }
    else if(id_number == "triangle"){
        canvas.style.cursor = `url('./icon_svg/triangle.svg'), auto`;
    }
    else if(id_number == "rectangle"){
        canvas.style.cursor = `url('./icon_svg/rectangle.svg'), auto`;
    }
    else if(id_number == "line"){
        canvas.style.cursor = `url('./icon_svg/line.svg'), auto`;
    }
    else if(id_number == "font"){
        canvas.style.cursor = `url('./icon_svg/font.svg'), auto`;
    }
}



function Pen(e){
    ctx.globalCompositeOperation = 'source-over';
    ctx.strokeStyle = temp_color.value
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.lineWidth = temp_linewidth.value;
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.closePath();
    ctx.stroke();
    [lastX, lastY] = [e.offsetX, e.offsetY];
}

function Colorful(e){
    ctx.globalCompositeOperation = 'source-over';
    ctx.strokeStyle = `hsl(${hue},100%,50%)`;
    hue <= 360 ? hue++ : hue = 0;
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.lineWidth = temp_linewidth.value;
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.closePath();
    ctx.stroke();
    [lastX, lastY] = [e.offsetX, e.offsetY];
}

function Eraser(e){
    ctx.globalCompositeOperation = 'destination-out';
    // ctx.strokeStyle = `rgba(255,255,255, 0)`;
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.lineWidth = temp_linewidth.value;
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.closePath();
    ctx.stroke();
    [lastX, lastY] = [e.offsetX, e.offsetY];
}

function Circle(e){
    ctx.globalCompositeOperation = 'source-over';
    ctx.strokeStyle = temp_color.value
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.lineWidth = temp_linewidth.value;
    // Animation();
    Circle_D(e);    
}

function Circle_D(e){
    var r = Math.sqrt(Math.pow((e.offsetX-lastX)/2,2) + Math.pow((e.offsetY-lastY)/2,2));
    var halfX = Math.abs(e.offsetX-lastX)/2;
    var halfY = Math.abs(e.offsetY-lastY)/2;
    ctx.beginPath();
    if(e.offsetX >= lastX){
        if(e.offsetY >= lastY){
            ctx.arc(lastX+halfX,lastY+halfY,r,0,Math.PI*2);
        }
        else{
            ctx.arc(lastX+halfX,lastY-halfY,r,0,Math.PI*2);
        }
    }
    else{
        if(e.offsetY >= lastY){
            ctx.arc(lastX-halfX,lastY+halfY,r,0,Math.PI*2);
        }
        else{
            ctx.arc(lastX-halfX,lastY-halfY,r,0,Math.PI*2);
        }
    }
    if(!isHollow){
        ctx.fillStyle=ctx.strokeStyle;
        ctx.fill();
    }
    else{
        ctx.closePath();
        ctx.stroke();
    }    
}

function Triangle(e){
    ctx.globalCompositeOperation = 'source-over';
    ctx.strokeStyle = temp_color.value
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.lineWidth = temp_linewidth.value;
    // Animation();
    Triangle_D(e);        
}

function Triangle_D(e){
    var halfE = Math.abs(e.offsetX-lastX)/2;
    ctx.beginPath();
    if(e.offsetX >= lastX){
        ctx.moveTo(lastX, e.offsetY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.lineTo(lastX+halfE, lastY);
        ctx.lineTo(lastX, e.offsetY);
    }
    else{
        ctx.moveTo(lastX, e.offsetY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.lineTo(lastX-halfE, lastY);
        ctx.lineTo(lastX, e.offsetY);
        
    }
    if(!isHollow){
        ctx.fillStyle= temp_color.value;
        ctx.fill();
    }
    else{
        ctx.closePath();
        ctx.stroke();
    }
}

function Rectangle(e){
    ctx.globalCompositeOperation = 'source-over';
    ctx.strokeStyle = temp_color.value
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.lineWidth = temp_linewidth.value;
    // Animation();
    Rectangle_D(e);
}

function Rectangle_D(e){
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(lastX, e.offsetY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.lineTo(e.offsetX, lastY);
    ctx.lineTo(lastX, lastY);
    if(!isHollow){
        ctx.fillStyle= temp_color.value;
        ctx.fill();
    }
    else{
        ctx.closePath();
        ctx.stroke();
    }
}

function Line(e){
    ctx.globalCompositeOperation = 'source-over';
    ctx.strokeStyle = temp_color.value
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.lineWidth = temp_linewidth.value;
    // Animation();
   Line_D(e);
}

function Line_D(e){
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.closePath();
    ctx.stroke();
}

function Animation() {
    if (step >= 0) {
        return promise =  new Promise((resolve, reject) => {
            var canvasPrev = new Image();
            canvasPrev.src = p_array[step];
            canvasPrev.onload = function() {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPrev,0,0);
                resolve();
            }
        });
    }
    else ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function Font(e){
    var textbox = document.createElement('input');
    var PosX = e.offsetX;
    var PosY = e.offsetY;
    var fontsize = temp_fontsize.options[temp_fontsize.selectedIndex].value;;
    var font = temp_font.options[temp_font.selectedIndex].value;
    isWriting = true;
    isDrawing = false;
    ctx.font = fontsize + "px " + font;
    ctx.fillStyle = temp_color.value;
    textbox.id = 'textBox';
    textbox.type = 'text';
    textbox.placeholder = "Type Your Word here";
    textbox.style.position = 'absolute';
    textbox.style.border = "1px #708069 double";
    textbox.style.borderRadius = "7px";
    textbox.style.left = PosX + 'px';
    textbox.style.top = PosY + 'px';
    document.body.appendChild(textbox);
    textbox.addEventListener('keydown', function(event){
        if(event.which == 13 || event.keyCode == 13){
            EnterWord(textbox.value, e);
            textbox.remove();
            document.body.removeChild(textbox);
        }  
    });
}

function EnterWord(content, e){
    ctx.fillText(content, e.offsetX, e.offsetY);
    isWriting = false;
    isDrawing = true;
    isFinished = true;
    Push();
}

function CleanTextbox(){
    let text = document.getElementById('textBox');
    text.remove();
  }

function Reset(){
    console.log("Reset the page?");
    alert("Reset the page?");
    ctx.globalCompositeOperation = 'source-over';
    ctx.clearRect(0,0,canvas.clientWidth,canvas.clientHeight);
    step = -1;
    CleanTextbox();
    Push();
}

//Push
function Push() {
    ctx.globalCompositeOperation = 'source-over';
    step++;
    if (step < p_array.length) { 
        p_array.length = step; 
    }
    p_array.push(canvas.toDataURL());
}

//download
function Download(){
    ctx.globalCompositeOperation = 'source-over';
    console.log("Download");
    var downloadimg = document.createElement("a");
    downloadimg.download = 'image.png';
    downloadimg.href = canvas.toDataURL("image/png");
    downloadimg.click();
}

//upload
function Upload(e){
    ctx.globalCompositeOperation = 'source-over';
    console.log("Upload");
    if(step == -1) Push();
    let reader = new FileReader();
    reader.onload = (e) =>{
        var img = new Image();
        img.onload = () =>{
            ctx.drawImage(img, 0, 0);
            Push();
        }
        img.src = e.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

//Undo
function Undo() {
    ctx.globalCompositeOperation = 'source-over';
    console.log("Undo");
    if (step > 0) {
        step--;
        var canvasPic = new Image();
        canvasPic.src = p_array[step];
        canvasPic.onload = () => {
            ctx.clearRect(0,0,canvas.clientWidth,canvas.clientHeight);
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
}

//Redo
function Redo() {
    ctx.globalCompositeOperation = 'source-over';
    console.log("Redo");
    if (step < p_array.length-1) {
        step++;
        var canvasPic = new Image();
        canvasPic.src = p_array[step];
        canvasPic.onload = () => { 
            ctx.clearRect(0,0,canvas.clientWidth,canvas.clientHeight);
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
}             

//Paint
function Paint(){
    if(isHollow) isHollow = false;
    else isHollow = true;
}

function ButtonChangeColor(btn){
    var collection = document.getElementsByClassName('flag');
    $.each(collection, function () {
        $(this).removeClass("end");
        $(this).addClass("button");
    });
    $(btn).addClass("end");
    $(btn).removeClass("button");
}

