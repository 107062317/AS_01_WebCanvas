# Reference

## Undo/Redo
http://www.htmleaf.com/ziliaoku/qianduanjiaocheng/201502151385.html


## Download
https://stackoverflow.com/questions/8126623/downloading-canvas-element-to-an-image


## Upload
http://www.htmleaf.com/ziliaoku/qianduanjiaocheng/201502151385.html


## Colorful pen
https://eruditeness.news.blog/2019/06/06/javascript%E5%B0%8F%E5%9E%8B%E5%B0%88%E6%A1%88%E7%B7%B4%E7%BF%9210-fun-with-html5-canvasjs30-8/


## Draw circle
https://stackoverflow.com/questions/21594756/drawing-circle-ellipse-on-html5-canvas-using-mouse-events


## Cursor style
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Basic_User_Interface/Using_URL_values_for_the_cursor_property


## Icon picture
https://www.flaticon.com/


## Select
https://robertsong.pixnet.net/blog/post/336117808


## Font
https://community.adobe.com/t5/animate/html5-canvas-dynamic-text-styling-and-overlapping-dynamic-text-boxes-not-lining-up/td-p/9928121?page=1
### Textbox
https://ithelp.ithome.com.tw/articles/10194138
### Enter keyboard
https://stackoverflow.com/questions/905222/enter-key-press-event-in-javascript


## Pen/other shape
https://ithelp.ithome.com.tw/articles/10194138


## Eraser
https://stackoverflow.com/questions/25907163/html5-canvas-eraser-tool-without-overdraw-white-color


## Change button color
https://blog.csdn.net/Mr_ccup/article/details/85613523?depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromBaidu-5&utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromBaidu-5
https://www.cnblogs.com/why-love-study/p/6025383.html
